'''
Mach Eight Sample Project

Thank you for taking the time to complete this sample project. We're a tech
first company and we value our engineers tremendously. We're are looking for
hard working, smart engineers with either excellent experience or lots of
potential.


Project

The project is to write a function that searches through NBA player heights
based on user input. The raw data is taken from
[here](https://www.openintro.org/data/index.php?data=nba_heights).  The data is
served in json format by the endpoint
[here](https://mach-eight.uc.r.appspot.com/).

The task is to create an application that takes a single integer input. The
application will download the raw data from the website above
(https://mach-eight.uc.r.appspot.com) and print a list of all pairs of players
whose height in inches adds up to the integer input to the application. If no
matches are found, the application will print "No matches found"

Sample output is as follows:

> app 139

- Brevin Knight         Nate Robinson
- Nate Robinson         Mike Wilks


The algorithm to find the pairs must be faster than O(n^2). All edge cases
should be handled appropriately. Though not strictly required, demonstrating
comfort in writing unit tests will make your submission stand out. This is
_not_ a closed book test. You are encouraged to reach out with any questions
that you come across.

Submission

The preferred form of submission is by publishing a public repo on github with
your code and a README file explaining how to run the code. I also can accept
an emailed zip file with the same contents.
'''

import urllib.request, urllib.parse, urllib.error
import json
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

sval=input("> app ")
try:
    num = int(sval)
except:
    print("Invalid input. Enter a numeric value.")
    quit()


url = 'https://mach-eight.uc.r.appspot.com'
handler = urllib.request.urlopen(url, context=ctx).read()

try:
    data = json.loads(handler)
except Exception as e:
    print("Invalid JSON format.")
    quit()

players = sorted(data['values'], key=lambda d: d['h_in'], reverse=True)

lspairs = list()
lenght = len(players)

for i in range(0,lenght):
    j = i + 1
    while j < lenght:
        if int(players[i]['h_in']) + int(players[j]['h_in']) >= num:
            lspairs.append((players[i],players[j]))
            print(players[i]["first_name"], players[i]["last_name"],'-', players[j]["first_name"], players[j]["last_name"])
            j = j + 1
        else:
            break

if not lspairs:
    print('No matches found.')
